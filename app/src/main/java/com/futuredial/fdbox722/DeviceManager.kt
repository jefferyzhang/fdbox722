package com.futuredial.fdbox722

import android.app.Activity
import android.app.admin.DeviceAdminReceiver
import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.os.Build
import android.os.Build.VERSION.SDK_INT

object DeviceManager {
    //private var dpm: DevicePolicyManager? = null
    //private var deviceCompName: ComponentName?=null

    fun getDpm(context: Context): DevicePolicyManager {
        return context.getSystemService(android.content.Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
    }

    fun getComponentName(context: Context): ComponentName {
        return ComponentName(context.applicationContext, Fdbox722Receiver::class.java)
    }

    fun grantPermissions(context: Context){
        if (SDK_INT > Build.VERSION_CODES.M) {
            getDpm(context).setPermissionGrantState(
                    getComponentName(context),
                    context.packageName,
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    DevicePolicyManager.PERMISSION_GRANT_STATE_GRANTED
            )
        }
    }

    fun IsAdmin(context: Context):Boolean{
        val mDPM = getDpm(context)
        return mDPM.isAdminActive(getComponentName(context))
    }

    fun RemoveAdmin(context: Context){
        val mDPM = getDpm(context)
        return mDPM.removeActiveAdmin(getComponentName(context))
    }

    fun ConfirmAdmin(context: Context){
        if (IsAdmin(context)){
            return
        }
        val intent = Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN)
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, getComponentName(context))
        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Must")
        //intent.flags = FLAG_ACTIVITY_NEW_TASK

        context.startActivity(intent)
    }

    fun WipeDevice(context: Context, flag:Int?){
        val mDPM = getDpm(context)
        if (mDPM.isAdminActive(getComponentName(context))){
            when {
                Build.VERSION.SDK_INT <28  -> {
                    if (flag==null){
                        //has SDcard EMMC
                        mDPM.wipeData(1)
                        Thread.sleep(40*1000)
                        mDPM.wipeData(0)
                    }else{
                        mDPM.wipeData(flag)
                    }
                }
                else    -> {
                    if (flag==null){
                        //has SDcard EMMC
                        mDPM.wipeData(0x7, "fd factoryreset")
                    }else{
                        mDPM.wipeData(flag, "fd factoryreset")
                    }
                }
            }

        }
    }
}