package com.futuredial.fdbox722

import android.app.ActivityManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import fi.iki.elonen.NanoHTTPD
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var myService: MyService? = null
    var isBound = false
    private val myConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName,
                                        service: IBinder) {
            val binder = service as MyService.MyLocalBinder
            myService = binder.getService()
            isBound = true
            GetBackgroundNotification(applicationContext, myService).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
        }
        override fun onServiceDisconnected(name: ComponentName) {
            isBound = false
        }
    }
    fun showTime() {
        val currentTime = myService?.getCurrentTime()
        Toast.makeText(this.applicationContext,currentTime.toString(),Toast.LENGTH_SHORT).show()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val server = AndroidWebServer(8080, this)
        server.start(NanoHTTPD.SOCKET_READ_TIMEOUT, true)

        val serviceClass = MyService::class.java
        val serviceIntent = Intent(applicationContext, serviceClass)
        // If the service is not running then start it
        if (!isServiceRunning(serviceClass)) {
            // Start the service
            startService(serviceIntent)
            bindService(serviceIntent, myConnection, Context.BIND_AUTO_CREATE)
        } else {
            Log.w("MainActivity","Service already running.")
            bindService(serviceIntent, myConnection, Context.BIND_AUTO_CREATE)
        }
        // Button to stop the service
        stop.setOnClickListener{
            //Unbind Service
            try {
                unbindService(myConnection)
            } catch (e: IllegalArgumentException) {
                Log.w("MainActivity", "Error Unbinding Service.")
            }
            // If the service is not running then start it
            if (isServiceRunning(serviceClass)) {
                // Stop the service
                stopService(serviceIntent)
            } else {
                Log.w("MainActivity","Service already stopped.")
            }
        }
        // Get the service status
        start.setOnClickListener{
            if (isServiceRunning(serviceClass)) {
                Log.w("MainActivity","Service is running.")
            } else {
                Log.w("MainActivity","Service is stopped.")
            }
            showTime()
        }
    }
    override fun onDestroy() {
        val serviceClass = MyService::class.java
        val serviceIntent = Intent(applicationContext, serviceClass)
        try {
            unbindService(myConnection)
        } catch (e: IllegalArgumentException) {
            Log.w("MainActivity", "Error Unbinding Service.")
        }
        if (isServiceRunning(MyService::class.java)) {
            stopService(serviceIntent)
        }
        super.onDestroy()
    }
    // Custom method to determine whether a service is running
    private fun isServiceRunning(serviceClass: Class<*>): Boolean {
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        // Loop through the running services
        for (service in activityManager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                // If the service is running then return true
                return true
            }
        }
        return false
    }

}
