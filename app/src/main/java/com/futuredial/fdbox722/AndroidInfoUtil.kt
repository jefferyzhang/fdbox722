package com.futuredial.fdbox722

import android.content.Context
import android.os.Build
import android.telephony.euicc.EuiccManager

class AndroidInfoUtil private constructor(private val context: Context?){
    private val boardName: String
        get() = Build.BOARD

    private val phoneInfo: String
        get() = Build.BOOTLOADER

    private val brand: String
        get() = Build.BRAND

    private val cpuAbil1: String
        get() = Build.CPU_ABI

    private val cpuAbil2: String
        get() = Build.CPU_ABI2

    private val sdkVersion: Int
        get() = Build.VERSION.SDK_INT

    private val sysVersion: String
        get() = Build.VERSION.RELEASE

    private val display: String
        get() = Build.DISPLAY

    private val model: String
        get() = Build.MODEL

    private val deviceName: String
        get() = Build.DEVICE

    private val manufature: String
        get() = Build.MANUFACTURER

    private val fingerprit: String
        get() = Build.FINGERPRINT

    private val serial: String
        get() = Build.SERIAL

    private val host: String
        get() = Build.HOST

    private val codeName: String
        get() = Build.ID

    override fun toString(): String {
        val stringBuilder = StringBuilder()
            stringBuilder.append("{")
                .append(""""boardName":""").append(boardName)
                .append(""""phoneInfo":""").append(phoneInfo)
                .append(""""brand":"""").append(brand)
                .append(""""cpuAbil1":""").append(cpuAbil1)
                .append(""""cpuAbil2":""").append(cpuAbil2)
                .append(""""sdkVersion":""").append(sdkVersion)
                .append(""""sysVersion":""").append(sysVersion)
                .append(""""display":""").append(display)
                .append(""""model":""").append(model)
                .append(""""deviceName":""").append(deviceName)
                .append(""""manufature":""").append(manufature)
                .append(""""fingerprit":""").append(fingerprit)
                .append(""""serial":""").append(serial)
                .append(""""host":""").append(host)
                .append(""""codeName":""").append(codeName)
                .append("}");
        return stringBuilder.toString()
    }


    companion object {
        private var androidinfo: AndroidInfoUtil? =null

        fun getAndroidInfoUtil(context: Context?): AndroidInfoUtil? {
            if(androidinfo == null){
                synchronized(AndroidInfoUtil::class.java){
                    androidinfo = AndroidInfoUtil(context)
                }
            }

            return  androidinfo;
        }
    }

}