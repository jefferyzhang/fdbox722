package com.futuredial.fdbox722

import android.app.admin.DeviceAdminReceiver
import android.app.admin.DevicePolicyManager
import android.content.Context
import android.content.Intent
import android.util.Log

class Fdbox722Receiver: DeviceAdminReceiver(){
    companion object {
        val TAG = "fdbox722Receiver"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.d(TAG, intent?.action)
        when (intent!!.action) {
            Intent.ACTION_BOOT_COMPLETED -> onBootCompleted(context)
            DevicePolicyManager.ACTION_DEVICE_OWNER_CHANGED -> onDeviceOwnerChanged(context)
            else -> super.onReceive(context, intent)
        }
    }

    override fun onEnabled(context: Context?, intent: Intent?) {
        super.onEnabled(context, intent)
        Log.d(TAG, "Owner enabled")
        context?.let {
            DeviceManager.grantPermissions(context)
        }
    }

    private fun onBootCompleted(context: Context?){
        Log.d(TAG, "Boot completed")
    }

    private fun onDeviceOwnerChanged(context: Context?) {
        Log.d(TAG, "Device owner changed")
    }
}