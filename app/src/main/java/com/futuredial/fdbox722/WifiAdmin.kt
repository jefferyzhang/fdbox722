package com.futuredial.fdbox722

import android.annotation.SuppressLint
import android.content.Context
import android.net.wifi.WifiManager
import android.os.Build
import android.text.TextUtils
import android.net.wifi.WifiInfo
import java.util.*
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.IOException
import java.net.NetworkInterface




class WifiAdmin private constructor(private val context: Context?){
    companion object {
        private var wifiadmin: WifiAdmin? = null
        private var wifimang: WifiManager? = null
        @SuppressLint("ServiceCast")
        fun CreateInstance(context: Context?): WifiAdmin {
            if(wifiadmin == null){
                synchronized(WifiAdmin::class.java){
                    wifiadmin = WifiAdmin(context)
                    wifimang = context!!.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager

                }
            }

            return  wifiadmin!!;
        }
    }

    fun openCloseWifi(enabled: Boolean):Boolean
    {
        when {
            wifimang?.isWifiEnabled!! -> return wifimang!!.setWifiEnabled(enabled)
        }
        return false
    }

    private fun getMacDefault(): String? {
        var mac:String?


        var info: WifiInfo? = null
        try {
            info = wifimang!!.connectionInfo
        } catch (e: Exception) {
        }

        mac = info!!.macAddress
        if (!TextUtils.isEmpty(mac)) {
            mac = mac.toUpperCase(Locale.ENGLISH)
        }
        return mac
    }

    private fun getMacAddress(): String {
        var WifiAddress = "02:00:00:00:00:00"
        try {
            WifiAddress = BufferedReader(FileReader(File("/sys/class/net/wlan0/address"))).readLine()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return WifiAddress
    }

    private fun getMacFromHardware(): String {
        try {
            val all = Collections.list(NetworkInterface.getNetworkInterfaces())
            for (nif in all) {
                if (nif.name !="wlan0") continue

                val macBytes = nif.hardwareAddress ?: return ""

                val res1 = StringBuilder()
                for (b in macBytes) {
                    res1.append(String.format("%02X:", b))
                }

                if (res1.isNotEmpty()) {
                    res1.deleteCharAt(res1.length - 1)
                }
                return res1.toString()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return "02:00:00:00:00:00"
    }

    fun GetMacAddress():String?{
        when(Build.VERSION.SDK_INT){
            in 0..Build.VERSION_CODES.LOLLIPOP_MR1-> return getMacDefault()
            in Build.VERSION_CODES.M..Build.VERSION_CODES.N-> return getMacAddress()
            else -> return getMacFromHardware()
        }
    }

}