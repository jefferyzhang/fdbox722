package com.futuredial.fdbox722

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.telephony.SubscriptionManager
import android.telephony.TelephonyManager
import android.telephony.euicc.EuiccManager
import android.util.Log

@RequiresApi(Build.VERSION_CODES.P)
class EUSimcard private constructor(private val context: Context?) {
    companion object {
        private lateinit var eusimard: EUSimcard
        private lateinit var eumanager: EuiccManager
        fun CreateInstance(context: Context?): EUSimcard {
            //if (checkNotNull(eusimard)){
                synchronized(EUSimcard::class.java){
                    eusimard = EUSimcard(context)

                    eumanager =  context!!.getSystemService(Context.EUICC_SERVICE) as EuiccManager
                }
            //}

            return  eusimard
        }
        fun IsSupportEusim(context: Context?):Boolean{
            return context?.getPackageManager()!!.hasSystemFeature(PackageManager.FEATURE_TELEPHONY_EUICC)
        }

        fun getSimInfo(context: Context?):Map<String, String?>{
            var aret = mutableMapOf<String, String?>()
            val telSubscriptionService = context!!.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE) as? SubscriptionManager
            val sims = telSubscriptionService?.accessibleSubscriptionInfoList
            sims?.forEach{
                Log.d("TAG", it!!.toString())
            }

            var support = IsSupportEusim(context)
            aret["IsSupportESIM"] = support.toString()
            Log.d("TAG", telSubscriptionService?.activeSubscriptionInfoCount.toString())
            aret["SIMCount"] = telSubscriptionService?.activeSubscriptionInfoCount.toString()
            val telephonyManager = context.getSystemService(Context.TELEPHONY_SERVICE) as? TelephonyManager
            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE)!=PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(context as Activity, arrayOf(android.Manifest.permission.READ_PHONE_STATE), 101)
            }
            Log.d("TAG", ""+telephonyManager?.phoneCount.toString())
            aret["SIMSlotCount"] = ""+telephonyManager?.phoneCount.toString()
            Log.d("TAG", ""+telephonyManager?.imei)
            aret["IMEI"]=""+telephonyManager?.imei
            Log.d("TAG", ""+telephonyManager?.meid)
            aret["MEID"] = ""+telephonyManager?.meid
            Log.d("TAG", ""+telephonyManager?.line1Number)
            aret["Line1Number"] = ""+telephonyManager?.line1Number
            Log.d("TAG", ""+telephonyManager?.subscriberId)
            aret["subscriberId"] = ""+telephonyManager?.subscriberId
            Log.d("TAG", ""+telephonyManager?.simSerialNumber)
            aret["simSerialNumber"] = ""+telephonyManager?.simSerialNumber

            return aret
        }
    }


    @RequiresApi(Build.VERSION_CODES.P)
    fun getEid():String?{
        return eumanager.eid
    }

    @RequiresApi(Build.VERSION_CODES.P)
    fun getEnabled():Boolean?{
        return eumanager.isEnabled
    }
}