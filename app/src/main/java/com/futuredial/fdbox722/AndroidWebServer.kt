package com.futuredial.fdbox722

import android.os.Build
import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.google.gson.JsonObject
import fi.iki.elonen.NanoHTTPD

class AndroidWebServer(port: Int, var context: MainActivity) : NanoHTTPD(port) {

    companion object {
        val TAG = "AndroidWebServer"
    }
    override fun serve(session: IHTTPSession?): Response {
        val sUrl = session?.uri
        val sMethod = session?.method
        Log.d(TAG, "url: $sUrl, Method: $sMethod")
        when(sUrl){
            "/did" -> return newFixedLengthResponse("worked!!")
            "/info" -> return getDeviceInfo(session)
            "/esiminfo" -> return getEUSIM(session)
            "/setadmin" -> return setAdmin(session)
            "/factoryreset" -> return factoryReset(session)
            else -> return newFixedLengthResponse(Response.Status.NOT_FOUND, MIME_PLAINTEXT, "Not Found!!")
        }
        //return super.serve(session)
        //return newFixedLengthResponse(Response.Status.FORBIDDEN, MIME_PLAINTEXT, "Error 403\r\nForbidden.")
    }

    private fun getDeviceInfo(session: IHTTPSession?): Response{
        val sInfo = AndroidInfoUtil.getAndroidInfoUtil(context).toString()
        return newFixedLengthResponse(sInfo)
    }

    private fun StringBuilder.serializeMap(data: Map<String, String?>){
        data.entries.joinToString (separator=",", prefix="{", postfix="}" ){ it->
            append(""""${it.key}"""")
            append(":")
            append(""""${it.value}"""")
        }
    }

    private fun getEUSIM(session: IHTTPSession?): Response{
        var mapinfo:Map<String, String?> = EUSimcard.getSimInfo(context)
        val gsonBuilder = GsonBuilder()
        val gson: Gson = gsonBuilder.registerTypeAdapter(JsonObject::class.java, JsonDeserializer<Any>{
            json, typeOfT, context -> json.asJsonObject
        }).disableHtmlEscaping().create()

        val sInfo = gson.toJson(mapinfo)
        return newFixedLengthResponse(sInfo)
    }

    private  fun  setAdmin(session: IHTTPSession?): Response{
        var sinfo :String
        if (DeviceManager.IsAdmin(context)){
            sinfo = """{"status":"success"}"""
        }else {
            DeviceManager.ConfirmAdmin(context)
            sinfo = """{"status":"confirm"}"""
        }
        return newFixedLengthResponse(sinfo)
    }


    private  fun factoryReset(session: IHTTPSession?): Response{
        var sflag = session!!.parameters["flag"]!!.get(0)
        DeviceManager.WipeDevice(context, sflag!!.toInt())
        val sinfo = """{"status":"success"}"""
        return newFixedLengthResponse(sinfo)
    }
}