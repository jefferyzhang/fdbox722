package com.futuredial.fdbox722

import android.bluetooth.BluetoothAdapter
import android.content.Context
import java.lang.reflect.AccessibleObject.setAccessible
import java.lang.reflect.Field
import java.lang.reflect.InvocationTargetException


class BTAdmin private constructor(private val context: Context?) {

    companion object {
        private var btadmin: BTAdmin? = null
        private var btAdapter: BluetoothAdapter? = null
        fun CreateInstance(context: Context?): BTAdmin {
            if(btadmin == null){
                synchronized(BTAdmin::class.java){
                    btadmin = BTAdmin(context)
                    btAdapter = BluetoothAdapter.getDefaultAdapter()
                }
            }

            return  btadmin!!;
        }
    }

    fun IsEnabled()= btAdapter?.isEnabled

    fun enableBluetooth():Boolean
    {
        btAdapter?.enable()
        var i=0
        while (btAdapter?.state != BluetoothAdapter.STATE_ON && i++ < 100 ){
            Thread.sleep(1000)
        }
        return btAdapter?.state == BluetoothAdapter.STATE_ON
    }

    fun disableBluetooth()= btAdapter?.disable()

    fun getBtAddressByReflection(): String? {
        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        //var field: Field? = null
        try {
            var field = BluetoothAdapter::class.java.getDeclaredField("mService")
            field.isAccessible = true
            val bluetoothManagerService = field.get(bluetoothAdapter) ?: return null
            val method = bluetoothManagerService.javaClass.getMethod("getAddress")
            val obj = method.invoke(bluetoothManagerService)
            return obj?.toString()
        } catch (e: NoSuchFieldException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: NoSuchMethodException) {
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        }

        return null
    }


}